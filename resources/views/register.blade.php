<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Register</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="http://code.jquery.com/qunit/qunit-1.15.0.css">
        <script src="http://code.jquery.com/qunit/qunit-1.15.0.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/register.css" rel="stylesheet">

    </head>
    <body>

    <div class="container mt-3">
        <input value={{$idCustomer + 1 }} id="customer_id" hidden>
        <div class="stepwizard col-md-offset-3">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                    <p>Personal Data</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Address</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>Payment Information</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>Submit Details</p>
                </div>
            </div>
        </div>
        <form role="form" action="" method="post" id='register-form'>
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="row setup-content" id="step-1">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Personal Data</h3>
                        <div class="form-group">
                            <label class="control-label">First Name</label>
                            <input maxlength="100" name="first_name" type="text" required="required" class="form-control" placeholder="Enter First Name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input maxlength="100" name="last_name" type="text" required="required" class="form-control" placeholder="Enter Last Name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telephone</label>
                            <input required="required" name="telephone" class="form-control" placeholder="Enter your telephone"/>
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3>Address</h3>
                        <div class="form-group">
                            <label class="control-label">Street</label>
                            <input maxlength="200" name="street" type="text" required="required" class="form-control" placeholder="Enter street name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">House Number</label>
                            <input maxlength="200" name="house_number" type="text" required="required" class="form-control" placeholder="Enter House Number">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Zip Code</label>
                            <input maxlength="200" name="zip_code" type="text" required="required" class="form-control" placeholder="Enter Zip Code">
                        </div>
                        <div class="form-group">
                            <label class="control-label">City</label>
                            <input maxlength="200" name="city" type="text" required="required" class="form-control" placeholder="Enter City">
                        </div>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3>Payment Information</h3>
                        <div class="form-group">
                            <label class="control-label">Account Owner</label>
                            <input maxlength="200" name="owner_name" type="text" required="required" class="form-control" placeholder="Owner name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">IBAN</label>
                            <input maxlength="200" name="iban" type="text" required="required" class="form-control" placeholder="IBAN">
                        </div>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-4">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Submit Details</h3>
                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                        <button class="btn btn-success btn-lg pull-right" id='btn-submit' type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="js/jquery-bootstrap-modal-steps.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="js/register/register.js"></script>
    </body>
</html>
