$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn'),
  		  allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allPrevBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.removeAttr('disabled').trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');


    let info = {};
  $('#register-form').submit( (e) => {
    e.preventDefault();

    $('#register-form').serializeArray().forEach((i) => {
        info[i.name] = i.value
    });
    bankInfo = {
        "customerId": $('#customer_id').val(),
        "iban": info.iban,
        "owner": info.owner_name,
    }
    console.log(JSON.stringify(bankInfo) )
    createRequestWunder(bankInfo)
  });

    let cookies = document.cookie.split(";");

    valCookies = {}

    if(!valCookies.first_name){
        cookies.forEach( value =>{
            value = value.split('=');
            console.log(value[0], value[1])
            valCookies[value[0].trim()] = value[1];
        });
    }

    if(valCookies.first_name && valCookies.last_name && valCookies.telephone){
        $('input[name="first_name"]').val(valCookies.first_name)
        $('input[name="last_name"]').val(valCookies.last_name)
        $('input[name="telephone"]').val(valCookies.telephone)
        $('#step-1').find('.nextBtn').click();
    }

    if(valCookies.street && valCookies.house_number && valCookies.zip_code){
        $('input[name="street"]').val(valCookies.first_name)
        $('input[name="house_number"]').val(valCookies.last_name)
        $('input[name="zip_code"]').val(valCookies.telephone)
        $('input[name="city"]').val(valCookies.city)
        $('#step-2').find('.nextBtn').click();
    }

    if(valCookies.street && valCookies.house_number && valCookies.zip_code){
        $('input[name="owner_name"]').val(valCookies.telephone)
        $('input[name="iban"]').val(valCookies.city)
        $('#step-3').find('.nextBtn').click();
    }
  });

  $('.nextBtn').on('click', () => {
    $('input').each((index, value) => {
        document.cookie = `${$(value).attr('name')}=${$(value).val()}`
    });
  });



let createRequestWunder = (bankInfo) => {

    setRequest('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', bankInfo)
    .then( data => {
        console.log(data);
    });
}

let createAddress = (info) => {
    setRequest('/register-payment-info', info)
    .then(data => {
        console.log(data)
//        createPersonalInfo(data.id)
    });
}

let createPersonalInfo = (address_id) => {
    data = {
        'address_id': address_id
    }
    setRequest('/register-personal-info', data)
    .then(data => {
        console.log(data.id)
    });
}

let setRequest = (urlRequest, Info) => {

    return new Promise( (resolve, reject) => {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: urlRequest,
            data: Info,
            success: (data) => {
                data = JSON.parse(data)
                resolve(data)
            },
            error: (error) => {
                reject(error)
            }
        })
    })
}
