## Questions to answer 

1.-Describe possible performance optimizations for your Code.

- I could implement full ECMAScript6 to have a better and much understandable code.
- Clean code, delete unnecessary code and reuse functions 
- if is possible, implement unit test to validate correct functionality 


2.-Which things could be done better, than you’ve done it?
- include comments, for an easier understanding of the code.
- show messages to the users when they do something wrong of if they finish everything correctly.

