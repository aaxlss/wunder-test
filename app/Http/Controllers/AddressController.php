<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\PersonalInfo;

class AddressController extends Controller
{
    public function setAddress(Request $request){

        if(DB::insert('insert into address (street, house_number, zip_code, city) values (?, ?, ?, ? )', [$request->input('street'), (int)$request->input('house_number'), $request->input('zip_code'), $request->input('city')])){
            $personalInfo = new PersonalInfo();
            $addressInfo = DB::table('address')->orderBy('id', 'DESC')->first();

            return $personalInfo->storeInfo($request, $addressInfo->id);
        }
        return false;
    }
}
