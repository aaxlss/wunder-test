<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalInfo extends Controller
{
    public function storeInfo ($info, $addressId){

        if(DB::insert('insert into personal_info (first_name, last_name, telephone, address_id) values (?, ?, ?, ? )', [$info->input('first_name'), $info->input('last_name'), $info->input('telephone'), $addressId])){
            return this.getLastId();
        }
        return false;
    }

    public function getLastId (){
        return json_encode(DB::table('personal_info')->orderBy('id', 'DESC')->first())
    }
}
